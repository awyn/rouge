module Tile where
import Data.List
import Data.Char

rgbToStr :: (Int, Int, Int) -> String
rgbToStr n = (r n) ++ ";" ++ (g n) ++ ";" ++ (b n) ++ "m"
  where r (x, _, _) = show x
        g (_, y, _) = show y
        b (_, _, z) = show z

_emojiMap :: String -> Char
_emojiMap name = case name of
  "mage" -> '\129497'
  "fairy" -> '\129498'
  "vampire" -> '\129499'
  "mermaid" -> '\129500'
  "elf" -> '\129501'
  "genie" -> '\129502'
  "zombie" -> '\129503'

emoji :: String -> (Int, Int, Int) -> String
emoji name bg = "\ESC[48;2;" ++ (rgbToStr bg) ++
                [_emojiMap name] ++ "\ESC[0m"

_charSet = "!\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"

_charIndex :: Char -> Int
_charIndex c = case find (\x -> (snd x) == c) (zip [0..] _charSet) of
  Just s -> fst s
  Nothing -> 0

_charMap :: Char -> Char
_charMap c = case c of
  ' ' -> chr (12288)
  otherwise -> chr (65281 + _charIndex c)

fwchar :: Char -> (Int, Int, Int) -> (Int, Int, Int) -> String
fwchar c fg bg = "\ESC[48;2;" ++ (rgbToStr fg) ++
               "\ESC[38;2;" ++ (rgbToStr bg) ++
               [_charMap c] ++ "\ESC[0m"
