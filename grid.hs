module Grid where

import Data.Array
import Data.List
import Data.List.Split
type Grid = Array (Int, Int) Int

-- === create ===
-- Create a grid of the provided dimensions and fill with a default supplied value
create :: Int -> Int -> Int -> Grid
create w h n = array ((0,0),(ww,hh)) [((x,y),n) | x <- [0..ww], y <- [0..hh]]
  where ww = w - 1; hh = h - 1

-- === view ===
-- Return a slice of the grid as a smaller grid with the supplied dimensions
view :: Int -> Int -> Int -> Int -> Grid -> Grid
view x y w h g = ixmap ((x,y),(x+w,y+h)) (\(nx,ny) -> (nx,ny)) g

-- === fill ===
-- Fill a specified rectangle inside a grid with the supplied value
fill :: Int -> Int -> Int -> Int -> Int -> Grid -> Grid
fill x y w h n g = g // [((xx, yy), n) | xx <- [x..(x+w-1)], yy <- [y..(y+h-1)]]

-- === merge ===
-- Merge two grids of the same size
-- Values from the second grid, greater than zero,
-- will overwrite values in the second grid
merge :: Grid -> Grid -> Grid
merge g1 g2 = g1 // [(c ((xx, yy), 0)) | xx <- [0..ww], yy <- [0..hh]]
  where ww = fst $ snd $ bounds $ g1
        hh = snd $ snd $ bounds $ g1
        c ((a, b), n) = if get a b g2 > 0 then g2v else g1v
          where g1v = ((a,b),(get a b g1))
                g2v = ((a,b),(get a b g2))

-- === print ===
-- Print a grid with the supplied mapping function
print :: (Int -> String) -> Grid -> String
print f g = concat $ intersperse "\n" $ map concat $ q
  where w = (fst $ snd $ bounds $ g) - (fst $ fst $ bounds $ g)
        q = (map . map) f $ chunksOf (succ w) $ elems g

-- === inside ===
-- Return whether or not the supplied x y position is inside the grid
inside :: Int -> Int -> Grid -> Bool
inside x y g = x >= 0 && y >= 0
  && x <= (fst $ snd $ bounds g)
  && y <= (snd $ snd $ bounds g)

-- === clip ===
-- Return whether or not the supplied rectangle is inside the grid
clip :: Int -> Int -> Int -> Int -> Grid -> Bool
clip x y w h g = x >= 0 && y >= 0
  && x + w <= (fst $ snd $ bounds g)
  && y + h <= (snd $ snd $ bounds g)

-- === set ===
-- Set value at position inside grid
set :: Int -> Int -> Int -> Grid -> Grid
set x y n g = g // [((x,y),n)]

-- === get ===
--- Get value at position inside grid
get :: Int -> Int -> Grid -> Int
get x y g = g ! (x,y)
